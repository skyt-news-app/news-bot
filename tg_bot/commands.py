from aiogram.types import BotCommand
from aiogram.types import ReplyKeyboardMarkup, KeyboardButton, InlineKeyboardMarkup, InlineKeyboardButton

COMMANDS = [
    BotCommand(command="go_on", description="Получить новости"),
    BotCommand(command="more_about_bot", description="Справка"),
    BotCommand(command="help", description="Возникают трудности"),
    BotCommand(command="about_us", description="О нас"),
    BotCommand(command="advice", description="Что следует добавить")
]

# Создаём кнопки клавиатуры
# Keyboard = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
# Keyboard.add(KeyboardButton("/go_on")).insert(KeyboardButton("/more_about_Bot"))
#
# # Создаём кнопки для второй клавиатуры
Keyboard2 = ReplyKeyboardMarkup(resize_keyboard=True)
Keyboard2.add(KeyboardButton("/back")).add(KeyboardButton("/help")).insert(KeyboardButton("/advice")).add(
    KeyboardButton("/about_us"))

# Создаём клавиатуру для команды /back
Keyboard3 = ReplyKeyboardMarkup(resize_keyboard=True)
Keyboard3.add(KeyboardButton("Посмотреть новости")).insert(KeyboardButton("Привет"))

# Создаём инлайн клавиатуру для команды /more_about_Bot
InlineKeyboard = InlineKeyboardMarkup(row_width=2)
Inlinebutton = InlineKeyboardButton("back", )
Inlinebutton2 = InlineKeyboardButton("help", url="https://skytdimon.github.io/skyt-test/#")
Inlinebutton3 = InlineKeyboardButton("advice", url="https://skytdimon.github.io/skyt-test/#")
Inlinebutton4 = InlineKeyboardButton("about_us", url="https://skytdimon.github.io/skyt-test/#")
InlineKeyboard.add(Inlinebutton2, Inlinebutton3, Inlinebutton4)

# Развилка при команде /more_about_Bot
COMMAND_MORE = """
    ● Справка:
    <b>back</b>  — <em>Назад.</em>
    <b>about_us</b>  — <em>O нас.</em>
    <b>advice</b>  — <em>Что следует добавить.</em>
    <b>help</b>  — <em>Если возникли трудности.</em>"""
