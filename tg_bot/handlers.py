# Импорт необходимых библиотек для работа хэндлеров

import logging
from aiogram import types

# Импорт комманд и клавиатур

from random import randint
from commands import COMMANDS, COMMAND_MORE, Keyboard3

# Импорт функций для парсинга

import json
from habr.habr import news_update_Habr
from RBK.RBK import news_update_RBK
from securitylab.secutirylab import news_update_securitylab

# Массив со стикерами

arr = ["CAACAgIAAxkBAAEIghhkMYfg0MiPEPeMndYGEPAfXBFYrgACex4AAppDsUlJom66e2D9SS8Eб",
       "CAACAgIAAxkBAAEIgnlkMaAEULyA1t_p6qS4hi5mFnmx8wAC3iQAAmu8wUtitGk9O1P0vS8E",
       "CAACAgQAAxkBAAEIgntkMaASOxcDoEooo9HyigABf8jxzukAApQAA-gKMS3jp43lQU8qhS8E",
       "CAACAgIAAxkBAAEIgn1kMaAYUkGOhoJraEJt7BHUwZ1KXAACQhAAAjPFKUmQDtQRpypKgi8E",
       "CAACAgIAAxkBAAEIgn9kMaAlxqMHGnXEEFyAzNtRNr--YgAC2msAAuCjggeGKQFPMFI9cS8E",
       "CAACAgIAAxkBAAEIgoFkMaAwYE1cgc3F9ZtgR81pM7np9wACghAAAqF3iUjfL4VGeCOsHi8E",
       "CAACAgIAAxkBAAEIgoNkMaA_HzD5RPzHj3Uqc-Puov3T0wAC0wADVp29CvUyj5fVEvk9LwQ",
       "CAACAgIAAxkBAAEIgoVkMaBUoZkZwvQElOyt78IwrIOYPQACZyQAAulVBRj5H4fx0MS3kC8E",
       "CAACAgIAAxkBAAEIgodkMaBmzRb0Bw8biZSZ2xR68pEg7AACPx4AAnwc8Eo2AR7FZ4oUaS8E",
       "CAACAgIAAxkBAAEIgpZkMaS6FoEj49JFyQPcDzjFxKGELQAClRQAAkrUKElv0CumT8RpsS8E",
       "CAACAgIAAxkBAAEIgphkMaTsNC6p2JbEqdmc5Vxg2k5AQwACM2kAAuCjggfyFNiGDUKxEy8E",
       "CAACAgIAAxkBAAEIhM5kMrxhk3cdZ_VQU95uCcpw90QZqAACT0AAAuCjggcuO1Eat5jdpy8E",
       "CAACAgIAAxkBAAEIhNNkMr26FV3HExkrWSkaetv7jCH3PgAC_BIAAjKs0Ej7_XN5qkuaci8E"]


def main(dp, bot):
    # Хендлер отлавливает и анализирует сообщения
    @dp.message_handler(commands=("start"), state='*')
    async def command_start(message: types.Message):
        RANDOM = int(randint(0, len(arr)))
        logging.info('start command')
        await bot.set_my_commands(commands=COMMANDS)
        await bot.send_sticker(message.from_user.id, sticker=arr[RANDOM])
        await message.answer("● <b>Skyt-News_bot</b> готов к работе!", parse_mode="HTML")
        await message.answer("● Для выбора действий используйте меню команд ⇩.", reply_markup=Keyboard3)

    @dp.message_handler(commands=("go_on"), state='*')
    async def command_go_on(message: types.Message):
        # Можно улучшить за счёт проверки последней новости

        news_update_Habr()
        await bot.set_my_commands(commands=COMMANDS)

        # Вывод самих новостей
        with open("../habr/news_dict_habr.json") as file:
            news_dict = json.load(file)
        for i, v in news_dict.items():
            news = f"Habr - IT\n" \
                   f" \n" \
                   f"{v['title']}\n" \
                   f" \n" \
                   f"{v['href']}"
            await message.answer(news)
        await message.answer(("Должно будет вылетать поле с возможностью выбора категорий, т.е. главная наша функция"))
        await message.delete()

    @dp.message_handler(commands=("more_about_Bot"), state='*')  # немного видоизменить так как с меню такая необходимость команды сильно упала
    async def command_more(message: types.Message):
        await bot.set_my_commands(commands=COMMANDS)
        await message.answer(text=COMMAND_MORE, parse_mode="HTML")  # , reply_markup=InlineKeyboard
        await message.delete()

    @dp.message_handler(commands=("back"), state="*")  # вообще не надо так как в меню уже всё есть
    async def command_back(message: types.Message):
        await bot.set_my_commands(commands=COMMANDS)
        await message.answer("● <em>Вы вернулись обратно</em>", parse_mode="HTML")  # , reply_markup=Keyboard3
        await message.delete()

    @dp.message_handler(commands=("help"), state='*')
    async def command_help(message: types.Message):
        await bot.set_my_commands(commands=COMMANDS)
        await message.answer("https://skytdimon.github.io/skyt-test/#")
        await message.delete()

    @dp.message_handler(commands=("advice"), state='*')
    async def command_advice(message: types.Message):
        await bot.set_my_commands(commands=COMMANDS)
        await message.answer("https://skytdimon.github.io/skyt-test/#")
        await message.delete()

    @dp.message_handler(commands=("about_us"), state='*')
    async def command_about_us(message: types.Message):
        await bot.set_my_commands(commands=COMMANDS)
        await message.delete()
        await message.answer("https://skytdimon.github.io/skyt-test/#")

    @dp.message_handler()
    async def command_tevirp(message: types.Message):
        if message.text == "Привет":
            RANDOM = int(randint(0, len(arr)))
            logging.info('start command')
            await bot.set_my_commands(commands=COMMANDS)
            await bot.send_sticker(message.from_user.id, sticker=arr[RANDOM])
            await message.answer("● <b>Skyt-News_bot</b> готов к работе!", parse_mode="HTML")
            await message.answer("● Для выбора действий используйте меню команд ⇩.", reply_markup=Keyboard3)
        elif message.text == "Посмотреть новости":
            await bot.set_my_commands(commands=COMMANDS)
            # Вывод самих новостей

            with open("../habr/news_dict_habr.json") as file:
                news_dict = json.load(file)
            for i, v in news_dict.items():
                news = f"Habr - IT\n" \
                       f" \n" \
                       f"{v['title']}\n" \
                       f" \n" \
                       f"{v['href']}"
                await message.answer(news)

            await message.delete()
