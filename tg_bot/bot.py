# Этот бот - это сервер, который взаимодействует с API Telegram.
# API - составляющая архитектуры Telegram и взаимодействуящая с нашим ботом.


from aiogram import Bot, Dispatcher, executor
import logging
from config import TOKEN_API
import handlers


# Оповещение о запуске бота (в терминале)
async def on_startup(_):
    print("Bot successfully launched!")


# Cоздадём экземпляр нашего бота
bot = Bot(TOKEN_API)

# Создаём Диспетчер, который принимает все апдейты
dp = Dispatcher(bot=bot)

# Включаем логирование, чтобы не пропустить важные сообщения
# Это значит, данный модуль будет запускаться самостоятельно
if __name__ == "__main__":
    handlers.main(dp, bot)
    logging.basicConfig(level=logging.INFO)
    executor.start_polling(dp, skip_updates=True, on_startup=on_startup)  # Запускаем бота
    # Оповещение о запуске бота (в терминале)
